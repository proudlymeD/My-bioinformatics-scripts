# My-bioinformatics-scripts

A bunch of bioinformatics or related scripts I made for genomic/epigenomic tasks. Feel free to use any of them. Don't expect them to work flawlessly tho, pleaes do your own due diligence and/or send an issue if you find something not working as expected.

Individual descriptions:

### C scripts:
* genome_n2bed.c: C script for finding the BED coordinates of all hard-masked (N) nucleotides in a fasta file. 
* genome_cpgs2bed.c: C script for finding BED coordinates of all CG dinucleoties. Very similar to the above script.

### R/python scripts:
* get_cpg_probes_hg19.R: R script for generating a BED file that maps Illumina CpG probes to genomic coordinates and strands. This only works for hg19, for hg38 please use UCSC's liftOver tool to convert. 

### Shell scripts:
* bedchopper.awk: AWK script that functions analogously to ``bedops --chop 1 <input_bed>``. 
* gtf2bed.sh: Converts .gtf files into .bed files, with optional sorting and regions merging (requires *bedtools* for merging).
* intersectidx.awk: Finds intersect between 1) a BED file and 2) any file with col1,2 as chr/pos. Similar to `bedtools intersect` but works with slightly different file inputs.
* process_noncoding.sh: Script taht 1) creates a temporary BED file that spans the entire lengths of chromosomes 1-22, X, and Y; and 2) removes BED coordinates from the previous temporary file to create BED coordinates for "noncoding background" regions of the human genome.
* pseudobulk_Me.sh: Concatenates, sorts, and adds records with duplicated genomic coordinates across certain fields (can be specified). See the in-file comments for more description. Can be used for creating methylation read-file pseudobulks.
* transposer.awk / transposer_slow.awk: Two awk scripts for transposing data, one loads the entire data in RAM (faster) and the other should work for larger datasets. Copied from [this Stack Overflow thread](https://stackoverflow.com/questions/1729824/an-efficient-way-to-transpose-a-file-in-bash) and I do not claim intellectual ownership to either script.
* summary_stats.sh: Returns summary statistics (N, mean, stdev, percentiles) given an input list of numbers. Only takes inputs from STDIN, and the percentiles are hard-coded to be "0 25 50 75 100" at this point. 
