#!/usr/bin/awk -f
# Match a BED file with a file which has col1/2 as chr/pos

(NR==FNR) {
  j[NR]=NR; chr[NR]=$1; start[NR]=$2; end[NR]=$3;
  next;
} 
{
  for (i in j) {
    if (($1 == chr[i]) && ($2 > start[i]) && ($2 <= end[i])) {
      print $0
    }
  }
}
