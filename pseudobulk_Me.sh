#!/bin/bash
# Make pseudobulks from input files based on common coordinates
# See helpinfo for more information

# Help function
helpinfo() {
  echo -e 'This scripts make pseudobulks by combining input compressed files.
Use -i to specify input files, and -o to specify output files.
Use -m/-u to specify methylated/unmethylated columns.'
}

# Get inputs
while getopts 'i:o:m:u:' OPTION; do
  case "$OPTION" in
    i)
      In="$OPTARG"
      echo "The input file/files are: $In"
      echo "The script assumes input files are compressesd (it also works even if they are not)."
      ;;
    o)
      Out="$OPTARG"
      echo "The output file is: $Out"
      ;;
    m)
      Valme="$OPTARG"
      echo "Use column $Valme for methylated counts."
      ;;
    u)
      Valunm="$OPTARG"
      echo "Use column $Valunm for unmethylated counts."
      ;;
    *)
      helpinfo
      exit 1
      ;;
  esac
done


# The first three lines combines, uncomments, 
# amd sorts the input data.
# then, notice that IF fields 1 and 2 are duplicated, 
# the script will add up methylated/unmethylated fields,
# and then re-calculate the division.
# Output will print out fields 1 and 2, methylated/unmethylated counts, sum, and ratio.
# This is meant for bisulfite sequencing read files.


zcat -f $In |\
  grep -v "#" |\
  sort -k1,1 -k2,2n -S 50% |\
  awk -v Cm="$Valme" -v Cu="$Valunm" 'BEGIN{ FS = OFS = "\t" }
    NR > 1 {
      if (($1 == f1) && ($2 == f2)) {
        $Cm += f3; $Cu += f4
      } else { print rec }
    }
    {
      total=$Cm+$Cu; ratio=$Cm/total
      rec = $1"\t"$2"\t"$Cm"\t"$Cu"\t"total"\t"ratio
      f1 = $1; f2 = $2; f3 = $Cm; f4 = $Cu
    }
    END{ print rec }' > $Out

echo "Done."

